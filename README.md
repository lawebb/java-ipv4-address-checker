# Java IPv4 address checker

This program will take 2 command line arguments of a filter and a hostname and
it will check that they are both IPv4 addresses


# Why I made this programm

I made this programm during my second year at university. This was my
introduction to networks.


# Features

    * Checks if two command line arguments are valid IPv4 addresses.
    * Reutrns true if the 2 addresses match and false if they do not.
    
    
# How to run on Linux

Set up directories as shown in my repository and cd into the root directory,
within the command line. The command should have two arguments. The first is
the filter and the second if the hostname.

    * $ javac *.java
    * $ java Coursework1 129.11.11.11 www.comp.leeds.ac.uk
    

# Working Examples

Example comparrisons:

![working](/images/working.png)


Example error outputs:

![errors](/images/error.png)


# Reports and Specification

System Spec:

[Link 1: Specification](https://gitlab.com/lawebb/java-ipv4-address-checker/-/blob/master/specification/coursework(2).pdf)

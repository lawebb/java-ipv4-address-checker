import java.net.*;

/* This class holds the object constructor and methods that will be used to
validate two IPv4 addresses and compare them against each other to determine
whether or not they match. */
public class Coursework1 {

  /* Boolean variable to signify the presence of wildcards. */
  private boolean wildcard;
  /* Integer variables to record integer versions of subnetworks (subNetwork),
  record wildcard positioning (wildcardCount) and to regulate for loops
  (i and j). */
  private int subNetwork, wildcardCount, i, j;
  /* String variables to store the filter (filter), hostname (hostname) and
  hostname address (address). */
  private String filter, hostname, address;
  /* InetAddress object to hold the hostname InetAddress. */
  private InetAddress inet;


  /* Constructor to initialise the object. */
  Coursework1(String filter, String hostname) {
    this.filter = filter;
    this.hostname = hostname;
    this.address = null;
  }


  /* Main method that calls the other methods. */
  public static void main(String[] args) {

    /* Initialising a new object, using the arguements given within the command
    line parameters for the program. */
    Coursework1 myObj = new Coursework1(args[0], args[1]);

    myObj.checkFirstSubnetwork();

    myObj.getHostnameAddress();

    myObj.addressComparison();

  }


  /* This method checks the first subnetwork of the filter address to ensure
  that it is valid. If it is not, then appropriate error messages are
  displayed. */
  private void checkFirstSubnetwork() {

    /* Split the filter by full stops. */
    String component[] = filter.split("[.]");

    /* Check that the first subnetwork is a number and that it is within the
    bounds. */
    try {

      subNetwork = Integer.parseInt(component[0]);

      if(subNetwork < 0 || subNetwork > 255) {
        System.out.println("Subnetwork 1 is out of bounds");
        System.exit(0);
      }
    }
    catch (NumberFormatException e) {
      System.out.println("Subnetwork 1 is not a number");
      System.exit(0);
    }

    checkRemSubnetworks(component);
  }


  /* This method checks the remaining filter address to ensure that it is a
  valid IPv4 address. If it is not, then appropriate error messages are
  displayed. */
  private void checkRemSubnetworks(String component[]) {

    /* Iterate over the 3 remaining subnetworks. */
    for(i=1; i<4; i++) {

      /* If the subnetwork is a wildcard, ensure that all subsequent subnetworks
      are also wildcards. */
      if(component[i].equals("*")) {

        /* Set the wildcard data to be used in comparison. */
        wildcard = true;
        wildcardCount = i;

        i++;

        for( ; i<4; i++) {

          if(!component[i].equals("*")) {
            j = i + 1;

            System.out.println("Subnetwork " + j + " is a value following a wildcard");
            System.exit(0);
          }
        }
      }
      /* If the subnetwork is not a wildcard, ensure that it is a number and
      that it is within the bounds. */
      else {

        try {
          subNetwork = Integer.parseInt(component[i]);

          if(subNetwork < 0 || subNetwork > 255) {
            j = i + 1;

            System.out.println("Subnetwork " + j + " is out of bounds");
            System.exit(0);
          }
        }
        catch (NumberFormatException e) {
          j = i + 1;

          System.out.println("Subnetwork " + j + " is neither a number or wildcard");
          System.exit(0);
        }
      }
    }
  }


  /* This method gets the Inet address of the hostname, checks that it is a
  valid IPv4 address and then assigns the address to the object. appropriate
  error messages are displayed where required. */
  private void getHostnameAddress() {

    /* Try to access the hostnames Inet address. */
    try {
      inet = InetAddress.getByName(hostname);

      /* Check if the inet address is an IPv4 address. */
      if(!(inet instanceof Inet4Address)) {
        System.out.println("hostname is not an IPv4 address");
        System.exit(0);
      }

      /* Assign the IPv4 address to our object. */
      String str1 = inet.toString();
      String divide[] = str1.split("/");

      address = divide[1];
    }
    catch(UnknownHostException e) {
      e.printStackTrace();
    }
  }


  /* This method compares the initial filter with the address of the hostname.
  It will return 'True' if the addresses match and 'False' if they do not. */
  private void addressComparison() {

    /* Split both strings up by full stops. */
    String cmp1[] = filter.split("[.]");
    String cmp2[] = address.split("[.]");

    /* If there was a wildcard present in the filter. */
    if(wildcard) {

      /* Iterate from i to the position at which the wildcard was found in the
      filter. */
      for(i=0 ; i<wildcardCount; i++) {

        /* If and of the subnetworks do not match, display 'False', else return
        'True'. */
        if(!(cmp1[i].equals(cmp2[i]))) {
          System.out.println("False");
          System.exit(0);
        }
      }

      System.out.println("True");
    }
    /* If there was no wildcard present in the filter, then check the filter
    against the hostname address directly. */
    else {
      if(filter.equals(address)){
        System.out.println("True");
      }
      else {
        System.out.println("False");
      }
    }
  }


}
